;; Emacs Shit
;; Use-Package
(defun use-package-normalize/:mark-selected (_name keyword args)
  (use-package-only-one (symbol-name keyword) args
    #'(lambda (_label arg) arg)))
(defun use-package-handler/:mark-selected (name _keyword selectedp rest state)
  (when selectedp (package--update-selected-packages `(,name) '()))

  (use-package-handler/:ensure name _keyword (list selectedp) rest state))

(with-eval-after-load 'use-package-core
  (setq use-package-always-defer t)

  (add-to-list 'use-package-keywords :mark-selected))

;; Packages
(use-package package
  :config (let* ((http      (concat (if (and (memq system-type '(windows-nt ms-dos))
                                             (not (gnutls-available-p)))
                                        "http"
                                      "https")
                                    "://"))
                 (cselpaURL (concat http "elpa.thecybershadow.net/packages/"))
                 (user42URL (concat http "download.tuxfamily.org/user42/elpa/packages/"))
                 ( melpaURL (concat http "melpa.org/packages/"))
                 (stableURL (concat http "stable.melpa.org/packages/"))
                 (   gnuURL (concat http "elpa.gnu.org/packages/")))
            (add-to-list 'package-archives (cons "melpa"         melpaURL) t)
            (add-to-list 'package-archives (cons "melpa-stable" stableURL) t)
            ;; (add-to-list 'package-archives (cons "user42"       user42URL) t)
            (add-to-list 'package-archives (cons "cselpa"       cselpaURL) t))
  :custom (package-archive-priorities '(;; ("marmalade"    .  50)
                                        ("cselpa"       .  50)
                                        ("gnu"          . 100)
                                        ;; ("user42"       . 150)
                                        ("melpa-stable" . 200)
                                        ("melpa"        . 250))))

;; (auto-package-update-maybe)

(use-package gnu-elpa-keyring-update
  :mark-selected t)


;; Loadpaths
(add-to-list 'load-path               "~/.emacs.d/lisp")
(add-to-list 'custom-theme-load-path  "~/.emacs.d/themes")
(add-to-list 'treesit-extra-load-path "~/.guix-profile/lib/tree-sitter/")

;; Customize
(use-package cus-edit
  :init   (when (file-exists-p custom-file) (load custom-file))
          ;; clears `package-selected-packages' so `use-package' can set it
          (customize-save-variable 'package-selected-packages nil)
  :custom (custom-file (concat user-emacs-directory "custom-init.el")))

;; Backups and Shit
(use-package files
  :custom (backup-directory-alist         '(("." . "~/.emacs.d/backup")))
          (auto-save-file-name-transforms '((".*" "~/.emacs.d/autosave/\\1" t)))
          (version-control                t  "Use version numbers on backups")
          (kept-new-versions              20 "how many of the newest versions to keep")
          (kept-old-versions              5  "and how many of the old")
          (delete-old-versions            t))

;; Desktops and Shit
(use-package desktop
  :init (setq desktop-dirname "~/.emacs.d/desktops"
              desktop-path    '("~/.emacs.d/desktops/")))

;; Window Shit
(use-package window
  :config   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ;; Custom screen splitting functions ;;
            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          (defun mine/vsplit-last-buffer ()
            (interactive)

            (split-window-vertically)
            (other-window 1 nil)
            (switch-to-next-buffer))
          (defun mine/hsplit-last-buffer ()
            (interactive)

            (split-window-horizontally)
            (other-window 1 nil)
            (switch-to-next-buffer))
  :bind   (("C-x 2" . mine/vsplit-last-buffer)
           ("C-x 3" . mine/hsplit-last-buffer)))

;; (require 'window-size)

;; Search Shit
(use-package grep
  :config (add-to-list 'grep-find-ignored-directories "node_modules")
          (add-to-list 'grep-find-ignored-directories "_build")
          (add-to-list 'grep-find-ignored-directories "deps")
          (add-to-list 'grep-find-ignored-files       "erl_crash.dump"))

;; (load-file "~/.emacs.d/lisp/globalff.el")
(use-package isearch
  :custom (search-upper-case  t)
          (isearch-lazy-count t))

;; Align Shit
(use-package align
  :config (add-to-list 'align-rules-list
                       '(symbol-value-alignment
                          (regexp . ":\\(\\s-*\\)")
                          (group  . 1)
                          (modes  . '(web-mode))
                          (repeat . nil)
                          (run-if . (lambda ()
                                      (or (string-suffix-p ".css"
                                                           buffer-file-name
                                                           t)
                                          (string-suffix-p ".scss"
                                                           buffer-file-name
                                                          t)))))))

;; Text Manipulation Shit
(use-package emacs
  :config (put 'upcase-region   'disabled nil)
          (put 'downcase-region 'disabled nil)

          (defun spacify ()
            "Replace every character of region with spaces except for the newlines."
            (interactive)

            (when mark-active
              (setq b (replace-regexp-in-string
                       "."
                       " "
                       (buffer-substring (region-beginning) (region-end))))

              (delete-region (region-beginning) (region-end))

              (insert b)))

          (defun mine/revert-buffer-no-confirm ()
            "Revert buffer without confirmation."
            (interactive)

            (message "Buffer reverting")
            (revert-buffer t t)
            (message "Buffer reverted"))

          (defun mine/insert-lambda ()
            (interactive)

            (insert-char (string-to-char "λ")))
  :bind   (("C-x M-r" . mine/revert-buffer-no-confirm)
           ("C-x 8 l" . mine/insert-lambda))
  :custom (scroll-preserve-screen-position t)
          (sentence-end-double-space       nil))

(use-package simple
  :bind   (("M-n"   .     next-logical-line)
           ("M-p"   . previous-logical-line)
           ("RET"   . newline-and-indent)
           ("C-j"   . newline-and-indent)
           ("M-j"   . join-next-line)
           ("M-RET" . default-indent-new-line))
  :config (defun join-next-line ()
            "Join this line to following and fix up whitespace at join.
If there is a fill prefix, delete it from the beginning of the following line."
            (interactive)

            (join-line -1))

          (defun mine/util-disable-indent-tabs ()
            "Turn off `indent-tabs-mode'.
The function toggles the mode which makes it tied to what the default is; this
allows us to know, for certain, it's been disabled."

            (setq indent-tabs-mode nil))
          (defun mine/util-enable-indent-tabs ()
            "Turn on `indent-tabs-mode'.
The function toggles the mode which makes it tied to what the default is; this
allows us to know, for certain, it's been enabled."

            (setq indent-tabs-mode t))
  :hook   ((emacs-lisp-mode . mine/util-disable-indent-tabs)
           (    elixir-mode . mine/util-disable-indent-tabs)
           (    scheme-mode . mine/util-disable-indent-tabs)
           (  c-mode-common . mine/util-enable-indent-tabs)
           (       lua-mode . mine/util-enable-indent-tabs)
           (        sh-mode . mine/util-enable-indent-tabs)
           (      text-mode . visual-line-mode))
  :custom (indent-tabs-mode                     nil)
          (column-number-mode                   t)
          (backward-delete-char-untabify-method nil
            "Delete tabs as single characters; don't convert to spaces."))

(use-package newcomment
  :bind (("C-M-j"      . indent-new-comment-line)
         ("<C-return>" . indent-new-comment-line)))

  ;; Mark Shit
(use-package rect
  :bind ("C-x C-SPC" . rectangle-mark-mode))

(use-package multiple-cursors
  :mark-selected t
  :bind          (("C-c m c"   . mc/edit-lines)
                  ("C-c m ."   . mc/mark-next-like-this)
                  ("C-c m ! ." . mc/unmark-next-like-this)
                  ("C-c m ,"   . mc/mark-previous-like-this)
                  ("C-c m ! ," . mc/unmark-previous-like-this)
                  ("C-c m SPC" . set-rectangular-region-anchor)))

(use-package delsel
  :custom (delete-selection-mode t
            "Typed text replaces the selection, if the selection is active."))

  ;; Braces Shit
(use-package elec-pair
  :config (defconst mine/-matching-parens
            '(( ?\( . ?\) )
              ( ?\[ . ?\] )
              ( ?\{ . ?\} )
              ( ?\< . ?\> )))

          (defun mine/-ppss-string-p (xs)
            "Non-‘nil’ if inside a string.
More precisely, this is the character that will terminate the
string, or ‘t’ if a generic string delimiter character should
terminate it."
            (elt xs 3))

          (defun mine/-open-paren (back-func)
            "Use BACK-FUNC to find an opening ( [ or { if any.
BACK-FUNC should be something like #\\='backward-sexp or #\\='backward-up-list."
            (save-excursion
              (ignore-errors
                (funcall back-func)
                (let ((ch (char-after)))
                  (and (eq ?\( (char-syntax ch))
                       ch)))))

          (defun mine/-self-insert (event)
            "Simulate a `self-insert-command' of EVENT.

Using this intead of `insert' allows self-insert hooks to run,
which is important for things like `electric-pair-mode'.

A command using this should probably set its \\='delete-selection
property to t so that `delete-selection-mode' works:

  (put \\='command \\='delete-selection t)

If necessary the value of the property can be a function, for
example `electric-pair-mode-not-active'."
            (let ((last-command-event event))     ;set this for hooks
              (self-insert-command (prefix-numeric-value nil))))

          (defun mine/insert-closing (&optional prefix)
            "Insert a matching closing delimiter.

With a prefix, insert the typed character as-is.

This is handy if you're not yet using `paredit-mode',
`smartparens-mode', or simply `electric-pair-mode' added in Emacs
24.5."
            (interactive "P")

            (if (eq 'org-mode major-mode)
                (call-interactively #'org-self-insert-command)
              (let* ((do-it (not (or prefix
                                     (and (string= "#\\"
                                                   (buffer-substring-no-properties
                                                    (- (point) 2) (point) )))
                                     (mine/-ppss-string-p (syntax-ppss)))))
                     (open-char  (and do-it        (mine/-open-paren #'backward-up-list)))
                     (close-pair (and open-char    (assq open-char mine/-matching-parens)))
                     (close-char (and close-pair   (cdr close-pair))))
                (mine/-self-insert (or close-char last-command-event)))))
  :bind   ((")" . mine/insert-closing)
           ("}" . mine/insert-closing)
           ("]" . mine/insert-closing)
           (">" . mine/insert-closing))
  :custom (electric-pair-mode              t)
          (electric-pair-inhibit-predicate #'electric-pair-conservative-inhibit))

;; Emacs Presentation/Display Shit
(use-package emacs
  :when   (display-graphic-p)
  :bind   ([f11] . switch-fullscreen)
  :config (defvar switch-fullscreen/last (cdr (assoc 'fullscreen
                                                     (frame-parameters))))

          (defun switch-fullscreen nil
            (interactive)

            (let* ((current (cdr (assoc 'fullscreen (frame-parameters))))
                   (predp   (eq current 'fullboth))
                   (nl      (if predp 1 -1)))
              (menu-bar-mode nl)
              (tool-bar-mode nl)
              (modify-frame-parameters (selected-frame)
                                       (list (cons 'fullscreen
                                                   (if predp switch-fullscreen/last
                                                             'fullboth))))

              (setq switch-fullscreen/last current))))

  ;; Margins Shit
(use-package marginalia
  :mark-selected t
  :custom        (marginalia-mode               t)
                 (marginalia-annotator-registry (assq-delete-all
                                                  'file
                                                  marginalia-annotator-registry))
  :bind          (:map minibuffer-local-map
                  ("M-q" . marginalia-cycle)))

  ;; Theme Shit
(use-package faces
  :custom-face (default ((t (:family "Fira Code" :height 130)))))

(use-package klere-theme
  :demand    t
  :load-path "themes/emacs-klere-theme"
  :config    (setq klere-interactively-modify-cursors-p        t
                   klere---isearch-hl-line-p                   t
                   klere---isearch-hl-line-major-modes-to-skip '(   wl-summary-mode
                                                                  gnus-summary-mode
                                                                 elfeed-search-mode))

             (load-theme 'klere t))

  ;; Mode-line
(use-package nyan-mode
  :mark-selected t
  :when          (display-graphic-p)
  :init          (nyan-mode t)
                 (nyan-start-animation))
(use-package doom-modeline
  :mark-selected t
  :config        (doom-modeline-def-segment my-word-count
                   "The buffer word count.

Displayed when in a major mode in `doom-modeline-continuous-word-count-modes'.
Respects `doom-modeline-enable-word-count'."
                   (when (and doom-modeline-enable-word-count
                              (member major-mode
                                      doom-modeline-continuous-word-count-modes))
                     (concat (doom-modeline-wspc)
                             (propertize (format-mode-line "Wd: ")
                                         'face '(:foreground "#B4BBC8"))
                             (propertize (format "%d" (count-words (point-min) (point-max)))
                                         'face (doom-modeline-face)))))

                 (doom-modeline-def-segment my-buffer-position
                   "The buffer position information."
                   (let ((visible    (doom-modeline--segment-visible 'buffer-position))
                         (mouse-face 'doom-modeline-highlight)
                         (local-map  mode-line-column-line-number-mode-map))
                     (concat
                       (doom-modeline-wspc)

                       ;; Line and column
                       (propertize (format-mode-line "Ln: ")
                                   'face       '(:foreground "#B4BBC8")
                                   'help-echo  "Buffer position\n\
mouse-1: Display Line and Column Mode Menu"
                                   'mouse-face mouse-face
                                   'local-map  local-map)
                       (propertize (format-mode-line "%l")
                                   'face       (doom-modeline-face)
                                   'help-echo  "Buffer position\n\
mouse-1: Display Line and Column Mode Menu"
                                   'mouse-face mouse-face
                                   'local-map  local-map)
                       (propertize (format-mode-line "  Col: ")
                                   'face       '(:foreground "#B4BBC8")
                                   'help-echo  "Buffer position\n\
mouse-1: Display Line and Column Mode Menu"
                                   'mouse-face mouse-face
                                   'local-map  local-map)
                       (propertize (format-mode-line "%c")
                                   'face       (if (> (current-column) 80)
                                                   `(:foreground "#F52749")
                                                 (doom-modeline-face))
                                   'help-echo  "Buffer position\n\
mouse-1: Display Line and Column Mode Menu"
                                   'mouse-face mouse-face
                                   'local-map  local-map)

                       ;; Position
                       (cond ((and visible
                                   (bound-and-true-p nyan-mode)
                                   (>= (window-width) nyan-minimum-window-width))
                                (concat (doom-modeline-wspc)
                                        (propertize (nyan-create) 'mouse-face
                                                                  mouse-face)))
                             ((and visible
                                   (bound-and-true-p poke-line-mode)
                                   (>= (window-width) poke-line-minimum-window-width))
                                (concat (doom-modeline-wspc)
                                        (propertize (poke-line-create) 'mouse-face
                                                                       mouse-face)))
                             ((and visible
                                   (bound-and-true-p mlscroll-mode)
                                   (>= (window-width) mlscroll-minimum-current-width))
                                (concat (doom-modeline-wspc)
                                        (let ((mlscroll-right-align nil))
                                          (format-mode-line (mlscroll-mode-line)))))
                             ((and visible
                                   (bound-and-true-p sml-modeline-mode)
                                   (>= (window-width) sml-modeline-len))
                                (concat (doom-modeline-wspc)
                                        (propertize (sml-modeline-create) 'mouse-face
                                                                          mouse-face)))
                             (t ""))

                       ;; Percent position
                       (when (and doom-modeline-percent-position (not (display-graphic-p)))
                         (concat
                           (doom-modeline-spc)
                           (doom-modeline-spc)
                           (doom-modeline-spc)
                           (propertize (format-mode-line '(""
                                                           doom-modeline-percent-position
                                                           "%%"))
                                       'face       (doom-modeline-face)
                                       'help-echo  "Buffer percentage\n\
mouse-1: Display Line and Column Mode Menu"
                                       'mouse-face mouse-face
                                       'local-map  local-map)))

                       (when (or line-number-mode
                                 column-number-mode
                                 doom-modeline-percent-position)
                         (doom-modeline-spc)))))

                 (doom-modeline-def-modeline 'main
                   `(eldoc ,(if (display-graphic-p) 'bar "") workspace-name window-number modals matches follow buffer-info remote-host
                           " " check my-word-count my-buffer-position parrot
                           ,(if (not (display-graphic-p)) "  " "") selection-info vcs)
                   '(compilation objed-state misc-info persp-name battery grip irc mu4e gnus github debug repl lsp input-method
                                 buffer-encoding "::" indent-info major-mode minor-modes process time))
  :custom        (doom-modeline-mode                   t)
                 (doom-modeline-project-detection      nil)
                 (doom-modeline-checker-simple-format  nil)
                 (doom-modeline-buffer-file-name-style 'truncate-upto-root)
                 (doom-modeline-height                 30)
                 ;; (doom-modeline-minor-modes            t)
                 (doom-modeline-enable-word-count      t)
                 (doom-modeline-indent-info            t))

;; Completing-Read Shit
(use-package ido
  :config      (setq ido-ignore-buffers (append
                                         '("\\*Completions\\*" "\\*Messages\\*"
                                           "\\*epc con"        "\\*Minibuf-"
                                           "*epc:server:"      "\\*code-conversion-work\\*"
                                           "\\*Echo Area"      "\\*VC-Git\\* tmp status-"   "\\*vc\\*")
                                         ido-ignore-buffers))

               (defun mine/read-from-kill-ring-wrapper (func &rest args)
                 (cl-letf* (((symbol-function 'complete-with-action)
                               (lambda (action collection string predicate)
                                 collection))
                            ((symbol-function 'completing-read)
                               (lambda (prompt collection &rest optionals)
                                 (apply #'ido-completing-read prompt
                                                              (funcall collection nil
                                                                                  nil
                                                                                  nil)
                                                              optionals))))
                   (apply func args)))
               (advice-add #'read-from-kill-ring
                           :around
                           #'mine/read-from-kill-ring-wrapper)

               ; Allow keys to work like dzen because I'm lazy
               (defun ido-local-keys ()
                 (unless (featurep 'edmacro)
                   (require 'edmacro))

                 (mapc (lambda (K)
                         (let ((key (car K)) (fun (cdr K)))
                           (define-key ido-completion-map
                                       (edmacro-parse-keys key)
                                       fun)))
                       '(("<right>" . ido-next-match) ("<left>"  . ido-prev-match)
                         ("<next>"  . ido-next-match) ("<prior>" . ido-prev-match)
                         ("<up>"    . ignore)         ("<down>"  . ignore))))
  :hook        (ido-setup . ido-local-keys)
  :bind        ("C-x C-b" . ido-switch-buffer)
  :custom      (ido-mode 'buffers "Turn on ido to change buffers with [C-x b]")
  :custom-face (ido-first-match ((t (:inherit 'font-lock-function-name-face)))))

(use-package all-the-icons-ivy-rich
  :mark-selected t
  :custom        (all-the-icons-ivy-rich-mode t))
(use-package ivy-rich
  :config (setq ivy-rich-display-transformers-list
                (thread-first
                  ivy-rich-display-transformers-list
                  (plist-put 'counsel-find-file
                             `(:columns   ((all-the-icons-ivy-rich-file-icon)
                                           (all-the-icons-ivy-rich-file-name              (:width 0.4))
                                           (all-the-icons-ivy-rich-file-id                (:width 15
                                                                                           :face  all-the-icons-ivy-rich-file-owner-face
                                                                                           :align right))
                                           (all-the-icons-ivy-rich-file-modes             (:width 12))
                                           (all-the-icons-ivy-rich-file-size              (:width 7
                                                                                           :face  all-the-icons-ivy-rich-size-face))
                                           (all-the-icons-ivy-rich-file-modification-time (:face  all-the-icons-ivy-rich-time-face)))
                               :delimiter "\t"))
                  (plist-put 'counsel-find-file-move
                             'ivy-rich--counsel-find-file-transformer)
                  (plist-put 'counsel-read-directory-name
                             'ivy-rich--counsel-find-file-transformer)))

          (ivy-rich-mode t)
  :custom (ivy-extra-directories nil))
(use-package ivy
  :bind   (:map ivy-minibuffer-map
           ("C-j"       . ivy-immediate-done)
           ("RET"       . ivy-alt-done)
           ("<backtab>" . ivy-previous-line))
  :custom (ivy-wrap t)
  :config ;; let the default for TAB be `ivy-next-line' instead of `ivy-alt-done'
          (defun ivy-partial-or-done/wrapper (func &rest args)
            (let ((this-command last-command))
              (cl-letf* (((symbol-function 'ivy-alt-done) 'ivy-next-line))
                (apply func args))))
          (advice-add #'ivy-partial-or-done
                      :around
                      #'ivy-partial-or-done/wrapper)

          (add-to-list 'ivy-re-builders-alist     (cons 'counsel-find-file #'regexp-quote))
          (add-to-list 'ivy-preferred-re-builders (cons #'regexp-quote     "quote")))

(use-package counsel
  :mark-selected t
  :commands      (counsel--find-file-matcher)
  :config        (defun mine/-counsel-find-extension ()
                   (if-let* ((input (let ((e (if (buffer-file-name)
                                                 (concat "*."
                                                         (file-name-extension
                                                           (buffer-file-name)))
                                               "all")))
                                      (read-string (concat "Search in files matching "
                                                           "wildcard (default " e "): ")
                                                   nil
                                                   nil
                                                   e)))
                             (alias (assoc input (progn
                                                   (unless (featurep 'grep)
                                                     (require 'grep))

                                                   grep-files-aliases))))
                       (cdr alias)
                     input))
                 (defun mine/counsel-rgrep-specify (dir ext)
                   (interactive (list (counsel-read-directory-name "Base directory: ")
                                      (mine/-counsel-find-extension)))

                   (mine/counsel-rgrep dir ext))
                 (defun mine/counsel-rgrep (dir ext)
                   (interactive (list (if-let ((d (locate-dominating-file
                                                    default-directory
                                                    ".git")))
                                          d
                                        (counsel-read-directory-name "Base directory: "))
                                      (mine/-counsel-find-extension)))

                   (if (locate-dominating-file dir ".git")
                       (pcase-let ((`(_ . ,cmd) (counsel--git-grep-cmd-and-proj nil)))
                         (counsel-git-grep nil dir (if (string-equal ext "* .*")
                                                       cmd
                                                     (format
                                                       "%s -- %s"
                                                       cmd
                                                       (shell-quote-argument ext)))))
                     (grep-compute-defaults)

                     (rgrep (read-string "Search for: " nil nil "") ext dir)))

                   ; source: http://steve.yegge.googlepages.com/my-dot-emacs-file
                 (defun mine/rename-file-and-buffer (filepath)
                   "Renames both current buffer and FILENAME it's visiting."
                   (interactive (list (buffer-file-name)))

                   (if filepath
                       (counsel-find-file-move filepath)
                     (message "Buffer '%s' is not visiting a file!" (buffer-name))))

                 (defun mine/find-file ()
                   (interactive)

                   (counsel-find-file (if (string-match "^/sudo:root@localhost:"
                                                        default-directory)
                                          (substring default-directory 21)
                                        default-directory)))
                 (defadvice read-only-mode (after read-only-mode-sudo activate)
                   "Find file as root if necessary."
                   (when (and (buffer-file-name)
                              (not (file-writable-p (buffer-file-name))))
                     (let ((p (point)))
                       (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))
                       (goto-char p))))
  :bind          (("C-c g"   . mine/counsel-rgrep)
                  ("C-c G"   . mine/counsel-rgrep-specify)
                  ("C-x M-w" . mine/rename-file-and-buffer)
                  ("C-x C-f" . mine/find-file)
                  :map counsel-git-grep-map
                  ("TAB"       . ivy-next-line)
                  ("<backtab>" . ivy-previous-line)))

;; Games Shit
  ;; Tetris Shit
(use-package tetris
  :config (setq tetris-score-file "~/.emacs.d/game_scores/tetris-scores"))
  ;; Snake Shit
(use-package snake
  :config (setq  snake-score-file "~/.emacs.d/game_scores/snake-scores"))
  ;; Minesweeper Shit
(use-package mines
  :mark-selected t)
  ;; 2048 Shit
(use-package 2048-game
  :mark-selected t)
  ;; Klondike Shit
(use-package klondike
  :mark-selected t
  :custom        (klondike-simplified-card-moving-p t))

;; Desktop Integration Shit
  ;; Auth. Shit
(use-package auth-source
  :custom (auth-sources '("secrets:Login" "~/.authinfo.gpg"
                          "~/.authinfo"   "~/.netrc")))

  ;; Alert Shit
(use-package alert
  :mark-selected t
  :config        (defun alert-dunstify-notify (info)
                   "Send INFO using dunstify.
Handles :ICON, :CATEGORY, :SEVERITY, :PERSISTENT, :NEVER-PERSIST, :TITLE
and :MESSAGE keywords from the INFO plist.  :CATEGORY can be
passed as a single symbol, a string or a list of symbols or
strings."

                   (let* ((args
                           (append
                            (list "--icon"    (or (plist-get info :icon) alert-default-icon)
                                  "--appname" "Emacs"
                                  "--urgency" (let ((urgency (cdr (assq
                                                                   (plist-get info :severity)
                                                                   alert-libnotify-priorities))))
                                                (if urgency
                                                    (symbol-name urgency)
                                                  "normal")))
                            (copy-tree alert-libnotify-additional-args)))
                          (category (plist-get info :category)))
                     (nconc args
                            (list "--timeout"
                                  (number-to-string
                                   (* 1000 ; notify-send takes msecs
                                      (if (and (plist-get info :persistent)
                                               (not (plist-get info :never-persist)))
                                          0 ; 0 indicates persistence
                                        alert-fade-time)))))
                     (when category
                       (nconc args
                              (list "--category"
                                    (cond ((symbolp category)
                                           (symbol-name category))
                                          ((stringp category) category)
                                          ((listp category)
                                           (mapconcat (if (symbolp (car category))
                                                          #'symbol-name
                                                        #'identity)
                                                      category ","))))))
                     (nconc args (list
                                  (alert-encode-string (plist-get info :title))
                                  (alert-encode-string (plist-get info :message))))
                     (apply #'call-process (executable-find "dunstify") nil
                            (list (get-buffer-create " *dunstify output*") t) nil args)))

                 (alert-define-style 'dunst :title    "Notify using dunst"
                                            :notifier #'alert-dunstify-notify)
  :custom        (alert-default-style 'dunst))

  ;; Dired Shit
(use-package dired
  :bind (:map dired-mode-map
         ("RET" . (lambda ()
                    (interactive)

                    (if (file-directory-p (dired-get-file-for-visit))
                        (call-interactively #'dired-find-alternate-file)
                      (call-interactively #'dired-find-file))))))

  ;; Clipboard Shit
(use-package menu-bar
  :config (defun mine/copy-to-clipboard ()
            (interactive)

            (if (display-graphic-p)
                (progn
                  (message "Yanked region to x-clipboard!")

                  (call-interactively 'clipboard-kill-ring-save))
              (if (not (region-active-p))
                  (message "No region active; can't yank to clipboard!")
                (call-process-shell-command
                  (concat "printf "
                          (s-replace "%"
                                     "%%"
                                     (shell-quote-argument
                                       (buffer-substring-no-properties
                                         (region-beginning)
                                         (region-end))))
                          " | wl-copy &"))  ; xsel -bi

                (message "Yanked region to clipboard!")
                (deactivate-mark))))

          (defun mine/paste-from-clipboard ()
            (interactive)

            (if (not (display-graphic-p))
                (if-let ((latest (shell-command-to-string "wl-paste --no-newline"))
                         (final  (+ (min (or (mark) (point-max)) (point))
                                    (length latest)))
                         (       (not (region-active-p))))
                    (insert latest)
                  (replace-region-contents (mark) (point) (lambda () latest))

                  (goto-char final))
              (clipboard-yank)

              (message "graphics active")))  ; xsel -b
  :bind   (([?\C-c ?c] .    mine/copy-to-clipboard)
           ([?\C-c ?v] . mine/paste-from-clipboard)))

;; Terminal Shit
(setq shell-file-name "bash")

(use-package xt-mouse
  :unless (display-graphic-p)
  :custom (xterm-mouse-mode t))

(use-package term-keys
  :demand        t
  :mark-selected t
  :config        (term-keys-mode t))

  ;; Eshell Shit
(use-package eshell
  :bind   ("C-x e" . eshell-or-new-session)
  :init   (setq   pcomplete-cycle-completions nil
                eshell-cmpl-cycle-completions nil)
  :config (defun eshell-or-new-session (&optional arg)
            "Create an interactive Eshell buffer.
If there is already an Eshell session active, switch to it.
If current buffer is already an Eshell buffer, create a new one and switch to it.
See `eshell' for the numeric prefix ARG."
            (interactive "P")

            (if arg
                (eshell arg)
              (if (eq major-mode 'eshell-mode)
                  (eshell t)
                (if-let ((latest (car (delq nil (mapcar
                                                  (lambda (x)
                                                    (and (string-prefix-p
                                                           eshell-buffer-name
                                                           (buffer-name x))    x))
                                                  (buffer-list))))))
                    (switch-to-buffer latest)
                  (eshell t)))))

          (defun number-to-string-approx-suffix (n &optional binary)
            "Return an approximate decimal representation of N as a string,
followed by a multiplier suffix (k, M, G, T, P, E, Z, Y). The representation
is, at most, 5 characters long for numbers between 0 and 10^19-5*10^16.

Uses a minus sign, if negative.

N may be an integer or a floating point number.

If the optional argument, BINARY, is non-nil, use 1024 instead of 1000 as the
base multiplier."

            (if (zerop n)
                "0"
              (let ((sign                                            "")
                    (b                            (if binary 1024 1000))
                    (suffix                                          "")
                    (bigger-suffixes '("k" "M" "G" "T" "P" "E" "Z" "Y")))
                (when (< n 0)
                  (setq n    (- n)
                        sign   "-"))

                (while (and (>= n b) (consp bigger-suffixes))
                  (setq n                             (/ n b)  ; TODO: this is rounding down; nearest would be better
                        suffix          (car bigger-suffixes)
                        bigger-suffixes (cdr bigger-suffixes)))

                (concat sign (if (integerp n)
                                 (int-to-string n)
                               (number-to-string (floor n))) suffix))))

          (setq eshell-prompt-function
                #'(lambda ()
                    (unless (featurep 'magit)
                      (require 'magit))

                    (let ((purple     '(:foreground "#75499B" :weight bold))  ; 622f7d
                          (gray       '(:foreground "#6D7070" :weight bold))  ; 434541
                          (white      '(:foreground "#FFFFFF"))
                          (bold-white '(:foreground "#FFFFFF" :weight bold))
                          (off-white  '(:foreground "#D1D1D1"))
                          (cyan       '(:foreground "#11D8E8" :weight bold))
                          (green      '(:foreground "#7BE128" :weight bold))
                          (yellow     '(:foreground "#EAB700" :weight bold))
                          (red        '(:foreground "#F52749" :weight bold))
                          (blue       '(:foreground "#007bb3" :weight bold)))
                      (concat
                        "\n"
                        (propertize "┌(" 'face gray)
                          (propertize default-directory 'face purple)
                          (propertize ")─{"             'face   gray)
                          (if (= eshell-last-command-status 0)
                              (if (cl-evenp (random 2))
                                  (concat (propertize "^" 'face bold-white)
                                          (propertize "‿" 'face  off-white)
                                          (propertize "^" 'face bold-white))
                                (concat (propertize "˘" 'face bold-white)
                                        (propertize "v" 'face  off-white)
                                        (propertize "˘" 'face bold-white)))
                            (if (cl-evenp (random 2))
                                (concat (propertize "ಥ" 'face blue)
                                        (propertize "_" 'face off-white)
                                        (propertize "ಥ" 'face blue))
                              (concat (propertize "╥"  'face blue)
                                      (propertize "﹏" 'face off-white)
                                      (propertize "╥"  'face blue))))
                          (propertize "}\n"             'face   gray)
                        (propertize "├" 'face gray)
                          (let ((branch-p (magit-get-current-branch)))
                            (if branch-p
                                (let ((edited-p (or
                                                   (magit-staged-files)
                                                   (magit-unstaged-files)
                                                   (magit-untracked-files))))
                                  (concat
                                    (propertize "["      'face gray)
                                    (propertize branch-p 'face (if edited-p red yellow))
                                    (propertize (let* ((behind-ahead  (if (delq
                                                                            nil
                                                                            (mapcar
                                                                              (lambda (ref)
                                                                                (and (string-prefix-p
                                                                                       "refs/remotes"
                                                                                       ref)            ref))
                                                                              (magit-list-refs)))
                                                                          (split-string
                                                                            (string-trim
                                                                              (shell-command-to-string
                                                                                (concat
                                                                                  "git rev-list --left-right "
                                                                                  "--count $(git for-each-ref "
                                                                                  "--format=\"%(upstream:short)\" "
                                                                                  "$(git symbolic-ref -q HEAD))..HEAD")))
                                                                            "\t")
                                                                        '("0" "0")))
                                                       ( ahead        (cadr behind-ahead))
                                                       (behind        (car  behind-ahead))
                                                       ( ahead-zero-p (string-equal ahead  "0"))
                                                       (behind-zero-p (string-equal behind "0")))
                                                  (if (or (not ahead-zero-p) (not behind-zero-p))
                                                      (concat
                                                        " "
                                                        (if  ahead-zero-p "" (concat "↑" ahead))
                                                        (if behind-zero-p "" (concat "↑" behind)))
                                                    "")) 'face (if edited-p red yellow))
                                    (propertize "]─"     'face gray)))
                              ""))
                          (propertize "(" 'face gray)
                            (let ((num-of-files (- (length
                                                     (directory-files default-directory)) 2)))
                              (concat
                                (propertize (number-to-string num-of-files) 'face bold-white)
                                (propertize " file"                         'face  off-white)
                                (propertize (if (= num-of-files 1) "" "s")  'face  off-white)
                                (propertize ", "                            'face  off-white)))
                            (propertize
                              (number-to-string-approx-suffix
                                (cl-reduce
                                  #'(lambda (result newCar)
                                      (+
                                        (if (string= (car newCar) "..")
                                            0
                                          (car (cddddr (cddddr newCar))))
                                        result))
                                  (directory-files-and-attributes
                                    default-directory)
                                  :initial-value 0))
                              'face
                              bold-white)
                            (propertize "iB" 'face off-white)
                            (propertize ")─" 'face      gray)
                          (propertize "(" 'face gray)
                            (propertize (getenv "USER")              'face      cyan)
                            (propertize "@"                          'face off-white)
                            (propertize (string-trim
                                          (shell-command-to-string
                                            "hostname"))             'face     green)
                            (propertize ")─"                         'face      gray)
                          (propertize "⟪" 'face gray)
                            " "
                            (let* ((curr                                              (current-time))
                                   (prev       (if (boundp 'my-resv-prev-time) my-resv-prev-time ""))
                                   (time (setq my-resv-prev-time (format-time-string "%-I:%M" curr))))
                              (concat (propertize time 'face (if (string-equal prev time)
                                                                 off-white
                                                               (append bold-white '(:slant italic))))
                                      " "
                                      (propertize (format-time-string "%P" curr) 'face off-white)))
                            " "
                            (propertize "⟫\n" 'face gray)
                        (propertize "└─>" 'face gray) (propertize " " 'face white))))
                eshell-prompt-regexp "└─> "
                eshell-history-size  660000)

          ;; pcomplete
          ;;;; sudo completion
          (defun pcomplete/sudo ()
            "Completion rules for the `sudo' command."
            (let ((completion-ignore-case t))
              (pcomplete-here (funcall pcomplete-command-completion-function))
              (while (pcomplete-here (pcomplete-entries)))))

          ;;;; man completion
          (defvar pcomplete-man-user-commands
            (split-string
             (shell-command-to-string
              "apropos -s 1 .|while read -r a b; do echo \" $a\";done;"))
            "p-completion candidates for `man' command")

          (defun pcomplete/man ()
            "Completion rules for the `man' command."
            (pcomplete-here pcomplete-man-user-commands)))
(use-package em-hist
  :bind  (:map eshell-hist-mode-map
          ("M-R" . eshell-isearch-backward)
          ("M-r" . move-to-window-line-top-bottom))
  :after (eshell))



;; Non-Programming Text Shit
(use-package ispell
  :bind ([f7] . ispell))

(use-package dictionary
  :bind ("C-c d" . dictionary-search))

(use-package google-translate
  :mark-selected t
  :init          (setq google-translate-translation-directions-alist '(("en" . "ht")
                                                                       ("ht" . "en")))
  :bind          (("C-c t" . google-translate-smooth-translate)
                  ("C-c T" . google-translate-query-translate)))

(use-package flyspell
  :hook (text-mode . flyspell-mode))

;; Reading Shit
(define-minor-mode reading-mode
  "Read a buffer with ease."
  :lighter " reading"
  :keymap  (let ((map (make-sparse-keymap)))
             (define-key map (kbd "n") #'next-line)
             (define-key map (kbd "f") #'forward-char)
             (define-key map (kbd "b") #'backward-char)
             (define-key map (kbd "p") #'previous-line)

             (define-key map (kbd "a") #'move-beginning-of-line)
             (define-key map (kbd "e") #'move-end-of-line)

             (define-key map (kbd "v") #'scroll-up-command)

             (define-key map (kbd "l") #'recenter-top-bottom)

             (define-key map (kbd "SPC") #'set-mark-command)

             (define-key map (kbd "<") #'beginning-of-buffer)
             (define-key map (kbd ">")       #'end-of-buffer)

             map)

  (read-only-mode))

;; Writing
(use-package fountain-mode
  :mark-selected t
  :bind          (:map fountain-mode-map
                  ("C-c C-o" . mine/fountain-read-with-evince))
  :config        (setq fountain-export-command-profiles
                       (cons (cons "screenplain" "screenplain --strong %b %B.pdf")
                             fountain-export-command-profiles))

                 (defun mine/fountain-read-with-evince ()
                   (interactive)

                   (call-process-shell-command
                     (concat "evince "
                             (substring buffer-file-name
                                        0
                                        (- (length buffer-file-name) 8))
                             "pdf")
                     nil
                     0)))

;; Book Shit
(use-package nov
  :mark-selected t
  :mode          ("\\.epub\\'" . nov-mode)
  :bind          (:map nov-mode-map
                  ("n" . next-line)
                  ("f" . forward-char)
                  ("b" . backward-char)
                  ("p" . previous-line)

                  ("A"      . nov-reopen-as-archive)
                  ("a"      . move-beginning-of-line)
                  ("<home>" . move-beginning-of-line)
                  ("e"      . move-end-of-line)
                  ("<end>"  . move-end-of-line)

                  ("V" . nov-view-source)
                  ("v" . scroll-up-command)

                  ("M-<left>"  . nov-history-back)
                  ("M-<right>" . nov-history-forward)
                  ("l"         . recenter-top-bottom)
                  ("r"         . move-to-window-line-top-bottom)

                  ("SPC" . set-mark-command)

                  :map nov-button-map
                  ("n" . next-line)
                  ("p" . previous-line)

                  ("A" . nov-reopen-as-archive)
                  ("a" . move-beginning-of-line)

                  ("V" . nov-view-content-source)
                  ("v" . scroll-up-command)

                  ("l" . recenter-top-bottom)

                  ("SPC" . set-mark-command)))

;; LaTeX Shit
; Make sure to install auctex (and texlive if installing auctex through the
; Emacs package manager) for any of this to work
(use-package auctex
  :mark-selected t
  :config        (defun LaTeX-insert-pair (&optional arg)
                   "Enclose following ARG sexps in LaTeX quotations (`` and '').
Leave point after open-paren."
                   (interactive "*P")

                   (if (not mark-active)
                       (progn
                         (insert-pair arg ?\` ?\')
                         (insert "`'")
                         (backward-char))
                     (progn
                       (setq re (region-end))
                       (setq rb (region-beginning))

                       (insert-pair arg ?\` ?\')

                       (goto-char (1+ rb))
                       (insert "`")

                       (goto-char (+ 2 re))
                       (insert "'")
                       (backward-char))))

                 (defun mine/LaTeX-init ()
                   (add-to-list 'TeX-view-program-list
                                '("Xreader" "xreader --page-index=%(outpage) %o"))
                   (add-to-list 'TeX-view-program-selection
                                '(output-pdf "Xreader")))
  :hook          (LaTeX-mode . mine/LaTeX-init)
  :bind          (:map LaTeX-mode-map
                  ("M-\"" . LaTeX-insert-pair))
  :custom        (TeX-auto-save  t)
                 (TeX-parse-self t)
                 (TeX-save-query nil)
                 (TeX-PDF-mode   t))

;; Markdown Shit
(use-package markdown-mode
  :mark-selected t)

;; Organization Shit
(use-package calendar
  :bind (("C-c C" . calendar)
         :map calendar-mode-map
         ("f" . calendar-forward-day)
         ("b" . calendar-backward-day)
         ("n" . calendar-forward-week)
         ("p" . calendar-backward-week)))

  ;; Org Shit
(use-package org
  :bind   (("C-c o a" . org-agenda)
           ("C-c o c" . org-capture)
           :map org-mode-map
           ("M-n"     . org-forward-heading-same-level)
           ("M-N"     . org-next-visible-heading)
           ("M-p"     . org-backward-heading-same-level)
           ("M-P"     . org-previous-visible-heading)
           ("C-c C-l" . mine/toggle-or-insert-link))
  :config (defun mine/toggle-or-insert-link ()
            ""

            (interactive)

            (if current-prefix-arg
                (call-interactively #'org-toggle-link-display)
              (call-interactively #'org-insert-link)))

          (defun mine/generate-current-day-org-file ()
            "Generates today's day as an Org file where I keep my notes."

            (concat "~/Documents/Orgs/" (format-time-string "%Y-%m-%d") ".org"))
          (defun mine/properly-tag (tag)
            ""

            (let ((fixed (concat ":@" tag ":")))
              (lambda ()
                (goto-char (point-max))

                (ignore-errors (search-forward fixed nil nil -1))

                (when (= (point) (point-max))
                  (goto-char (point-min)))

                (replace-regexp "^\\*\\* \\(.*\\)$" (concat "** \\1 " fixed)))))
          (defun mine/add-scheduling ()
            ""

            (goto-char (point-max))

            (search-backward "\nDEADLINE: ")
            (search-forward  "\nDEADLINE: ")
            (forward-char)

            (let ((p (point)))
              (search-forward ">")
              (backward-char 5)

              (let ((date (buffer-substring-no-properties p (point))))
                (goto-char (line-end-position))

                (insert "\nSCHEDULED: "
                        (format-time-string "<%Y-%m-%d %a +2w>"
                                            (time-add (date-to-time date)
                                                      -15552000))))))
  :custom (org-tag-alist                     '(;; Politics
                                               ("@donate"      . ?D)
                                               ("@boycott"     . ?B)
                                               ("@syndicalist" . ?S)))
          (org-tags-exclude-from-inheritance '("@donate" "@boycott" "@syndicalist"))
          (org-capture-templates             `(("d" "Mark Donation Plans")
                                               ("dp" "primary" entry
                                                (file ,#'mine/generate-current-day-org-file)
                                                "* TODO Donate to %?'s primary challenger :@donate:\nDEADLINE: %^t\n** "
                                                :before-finalize ,#'mine/add-scheduling)
                                               ("ds" "support" entry
                                                (file ,#'mine/generate-current-day-org-file)
                                                "* TODO Donate to %?'s campaign :@donate:\nDEADLINE: %^t\n** "
                                                :before-finalize ,#'mine/add-scheduling)

                                               ("b" "Businesses and Corporations")
                                               ("bb" "boycott" entry
                                                (file+headline ,#'mine/generate-current-day-org-file
                                                               "Businesses to Avoid")
                                                "** %?"
                                                :before-finalize ,(mine/properly-tag "boycott"))
                                               ("bc" "collectively owned" entry
                                                (file+headline ,#'mine/generate-current-day-org-file
                                                               "Alternatives to Corporations or Collectively Owned")
                                                "** %?"
                                                :before-finalize ,(mine/properly-tag "syndicalist")))))



;; Programming Text Shit
(use-package hideshow
  :hook ((scheme-mode . hs-minor-mode)
         (   web-mode . hs-minor-mode)))
(use-package hideshow
  :after  web-mode
  :config (let ((ext (file-name-extension (buffer-name))))
            (cond
             ((or (string-equal ext "css")
                  (string-equal ext "php")) (setq-local comment-start "/* "
                                                        comment-end   " */"))
             ((string-equal ext "js")       (setq-local comment-start "// "
                                                        comment-end   ""))
             ((or (string-equal ext "html")
                  (string-equal ext "xml")) (setq-local comment-start "<!-- "
                                                        comment-end   " -->"))))

          (defun unnec1 ()
            (interactive)

            (ignore-errors (web-mode-fold-or-unfold))
            (hs-hide-block))
          (defun unnec2 ()
            (interactive)

            (ignore-errors (web-mode-fold-or-unfold))
            (hs-show-block))
  :bind   (:map web-mode-map
           ("C-x w f" . unnec1)
           ("C-x w s" . unnec2)))

;; Auto-complete Shit
(use-package company
  :config (defun mine/company-javascript-setup ()
            (when (or (string-equal (file-name-extension buffer-file-name) "js")
                      (string-equal (file-name-extension buffer-file-name) "jsx"))
              (company-mode t)

              (setq company-tooltip-align-annotations t)))
  :bind   (:map company-active-map
           ("C-n" . nil)
           ("C-p" . nil)
           ("M-n" . #'company-select-next)
           ("M-p" . #'company-select-previous)
           :map company-search-map
           ("C-n" . nil)
           ("C-p" . nil)
           ("M-n" . #'company-select-next)
           ("M-p" . #'company-select-previous))
  :hook   (emacs-lisp-mode
           restclient-mode
           (web-mode . mine/company-javascript-setup)))

(use-package auto-complete
  :hook ((       scheme-mode . auto-complete-mode)
         (ejc-sql-minor-mode . auto-complete-mode)))

(use-package yasnippet
  :mark-selected t
  :config        (defun mine/yasnippet-javascript-setup ()
                   (when (or (string-equal (file-name-extension (buffer-name)) "js")
                             (string-equal (file-name-extension (buffer-name)) "jsx")
                             (string-equal (file-name-extension (buffer-name)) "scss"))
                     (yas-minor-mode-on)))
  :hook          ((      web-mode . mine/yasnippet-javascript-setup)
                  (python-ts-mode . yas-minor-mode-on)
                  (     java-mode . yas-minor-mode-on)
                  (   elixir-mode . yas-minor-mode-on)))

;; LSP Shit
(use-package lsp-mode
  :config (defun mine/lsp-scss-setup ()
            (when (string-equal (file-name-extension (buffer-name)) "scss")
              (add-to-list 'lsp-language-id-configuration '("\\.scss$" . "scss"))

              (setq lsp-headerline-arrow #("›" 0 1 (face #1=(:family  "Material Icons"
                                                             :height  1.0
                                                             :inherit lsp-headerline-breadcrumb-separator-face)
                                                         font-lock-face #1# display
                                                         (raise 0.0)
                                                         rear-nonsticky t)))

              (lsp)))
  :hook   ((      web-mode . mine/lsp-scss-setup)
           (python-ts-mode . lsp)
           (     java-mode . lsp)
           (   elixir-mode . lsp)))

(use-package lsp-ui
  :mark-selected t
  :bind          (:map lsp-ui-mode-map
                  ([remap xref-find-references] . lsp-ui-peek-find-references)
                  ("C-c l"                      . lsp-ui-imenu)
                  :map lsp-ui-imenu-mode-map
                  ("n" .     next-line)
                  ("p" . previous-line))
  :custom        (lsp-eldoc-render-all             t)
                 (lsp-ui-sideline-show-diagnostics nil))


;; Tramp Shit
;; (use-package tramp
;;   :
;; (require 'tramp)

;; ;; (setq tramp-default-method "scp")
;; (setq recentf-auto-cleanup 'never)

;; (add-to-list 'tramp-connection-properties      (list
;;                                                  (regexp-quote "192.168.1.109")
;;                                                  "remote-shell"
;;                                                  "sh"))
;; ;; (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
;; (add-to-list 'tramp-remote-path                "/system/xbin")
;; (add-to-list 'tramp-remote-process-environment "TMPDIR=$HOME")


;; Coding Shit?
(require 'smart-tabs-mode)

;; (smart-tabs-add-language-support web    web-mode-hook
;;   ((web-mode-indent-line . web-mode-code-indent-offset)))
(smart-tabs-add-language-support bash   sh-mode-hook
  ((sh-indent-line       . sh-basic-offset)
   (sh-basic-indent-line . sh-basic-offset)))
(smart-tabs-add-language-support lua    lua-mode-hook
  ((lua-indent-line . lua-indent-level)))
(smart-tabs-add-language-support ceylon ceylon-mode-hook
  ((ceylon-indent-line   . tab-width)
   (ceylon-format-region . tab-width)))
(smart-tabs-add-language-support scala  scala-mode-hook
  ((scala-indent:indent-line . scala-indent:step)))
(smart-tabs-add-language-support rust   rust-mode-hook
  ((rust-mode-indent-line . rust-indent-offset)))
;; (add-hook     'ruby-mode-hook (lambda ()
;;                                 (setq ruby-use-smie nil)))
;; (add-hook 'enh-ruby-mode-hook (lambda ()
;;                                 (setq ruby-use-smie nil)))
;; (smart-tabs-add-language-support ruby   ruby-mode-hook
;;   ;; ((ruby-indent-line . ruby-indent-level))
;;   ((smie-indent-line . ruby-indent-level)))
;; (smart-tabs-add-language-support enh-ruby enh-ruby-mode-hook
;;   ((enh-ruby-indent-line . ruby-indent-level)))

(smart-tabs-insinuate 'c 'c++ 'java 'python 'javascript 'ceylon 'scala 'rust ;; 'lua 'ruby 'enh-ruby
                      )

;; LISP Shit
  ;; Elisp Shit
(use-package elisp-mode
  :bind (:map emacs-lisp-mode-map
         ("C-c SPC" . align-let)))

  ;; Scheme Shit
(use-package geiser-guile
  :mark-selected t
  :custom        (geiser-guile-extra-keywords
                  '("return-if"                    "case-pred"
                    "if-let"                       "if-let*"
                    "if-let-helper"                "process-user-account-as"
                    "if-activityStream"            "generate-masto-object"
                    "generate-masto-object-helper" "insert-entity"
                    "users-define"                 "auth-define"
                    "main-define"                  "settings-define"
                    "profiles-define"              "search-define"           "let/cc")))
(use-package geiser
  :custom (geiser-default-implementation 'guile)
  :hook   (scheme-mode . geiser-mode))
(use-package ac-geiser
  :mark-selected t
  :hook          (scheme-mode . ac-geiser-setup))
(use-package scheme
  :config (put 'if         'scheme-indent-function 2)
          (put 'if-let     'scheme-indent-function 2)
          (put 'if-let*    'scheme-indent-function 2)
          (put 'case-pred  'scheme-indent-function 2)
          (put 'stream-let 'scheme-indent-function 2)
  :bind   (:map scheme-mode-map
           ("C-x g f" . hs-hide-block)
           ("C-x g s" . hs-show-block)
           ("C-c SPC" . align-let))
  :custom (scheme-program-name "guile"))

;; Web Shit
(use-package web-mode
  :mark-selected t
  :mode          ("\\.js\\'"       "\\.php\\'"
                  "\\.jsp\\'"      "\\.tpl\\.php\\'"
                  "\\.as[cp]x\\'"  "\\.html\\.eex\\'"
                  "\\.erb\\'"      "\\.html\\.heex\\'"
                  "\\.mustache\\'" "\\.html\\.leex\\'"
                  "\\.xml?\\'"     "\\.phtml\\'"
                  "\\.html?\\'"    "\\.djhtml\\'"
                  "\\.css\\'"      "\\.scss\\'"        "\\.tpl\\'")
  :bind          (:map web-mode-map
                  ("C-c SPC" . mine/web-align))
  :config        (defun mine/web-align ()
                   (interactive)

                   (let ((indent-tabs-mode nil))
                     (call-interactively #'align)))

                 (defun mine/web-tab-width ()
                   (setq tab-width 2))
  :hook          (web-mode . mine/web-tab-width)
  :custom        ((web-mode-enable-css-colorization t)
                  (web-mode-enable-auto-closing     t)
                  (web-mode-enable-auto-pairing     t)
                  (web-mode-enable-auto-quoting     t)
                  (web-mode-style-padding           2)
                  (web-mode-markup-indent-offset    2)
                  ;; (web-mode-attr-indent-offset      2)  ; Html attribute indentation level
                  (web-mode-script-padding          2)
                  (web-mode-code-indent-offset      2)
                  (web-mode-css-indent-offset       2)
                  ;; (web-mode-sql-indent-offset       2)  ; Sql (inside strings) indentation level
                  ))

  ;; HTML Shit
(use-package ac-html
  :mark-selected t
  :pin           "melpa-stable"
  :config        (defun mine/ac-html-setup ()
                   (when (or (string-equal (file-name-extension buffer-file-name) "html")
                             (string-equal (file-name-extension buffer-file-name) "tpl"))
                     (ac-html-enable)

                     (add-to-list 'ac-modes 'web-mode)

                     (auto-complete-mode t)))
  :hook          (web-mode . mine/ac-html-setup))

  ;; JavaScript Shit
(use-package flycheck
  :config (setq-default flycheck-disabled-checkers
                        (append flycheck-disabled-checkers
                                '(javascript-jshint json-jsonlint)))

          (flycheck-add-mode 'javascript-eslint 'web-mode)

          (defun mine/flycheck-javascript-setup ()
            (when (or (string-equal (file-name-extension buffer-file-name) "js")
                      (string-equal (file-name-extension buffer-file-name) "jsx"))
              (flycheck-mode  +1)))
  :hook   (web-mode . mine/flycheck-javascript-setup)
  :custom (flycheck-temp-prefix ".flycheck"))
(use-package tide
  :mark-selected t
  :config        (defun mine/tide-javascript-setup ()
                   (flycheck-add-mode 'javascript-tide 'web-mode)

                   (when (or (string-equal (file-name-extension buffer-file-name) "js")
                             (string-equal (file-name-extension buffer-file-name) "jsx"))
                     (tide-setup)))
  :hook          (web-mode . mine/tide-javascript-setup)
  :custom        (tide-tsserver-process-environment '("TSS_LOG=-level verbose -file /tmp/tss.log")))
(use-package eldoc
  :config (defun mine/eldoc-javascript-setup ()
            (when (or (string-equal (file-name-extension buffer-file-name) "js")
                      (string-equal (file-name-extension buffer-file-name) "jsx"))
              (eldoc-mode +1)))
  :hook   (web-mode . mine/eldoc-javascript-setup))

  ;; REST Requests Shit
(use-package company-restclient
  :mark-selected t
  :config        (add-to-list 'company-backends 'company-restclient)
  :after         restclient)
(use-package restclient
  :mode ("\\.restclient\\'" . restclient-mode))

;; Python Shit
(use-package python
  :init   (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))
  :config (defun mine/python-setup ()
            (setq tab-width 4)

            (add-to-list 'treesit-font-lock-settings
                         (car (treesit-font-lock-rules
                                :language 'python
                                :override t
                                :feature  'escape-sequence  ; font-lock-constant-face
                                '((escape_sequence) @elisp-shorthand-font-lock-face)))
                         t))
  :hook   ((python-ts-mode . mine/python-tab-width)))

;; Lua Shit
(use-package lua-mode
  :disabled      t
  :mark-selected t
  :mode          ("\\.lua$" . lua-mode)
  :interpreter   "lua"
  :config        (defun mine/lua-setup ()
                   (setq lua-indent-level 3
                         tab-width        3))
  :hook          (lua-mode . mine/lua-setup))

;; Shell Shit
(use-package sh-script
  :config (defun gker-setup-sh-mode ()
            "My own personal preferences for `sh-mode'.

This is a custom function that sets up the parameters I usually
prefer for `sh-mode'.  It is automatically added to
`sh-mode-hook', but is can also be called interactively."
            (interactive)

            (setq sh-basic-offset  2
                  tab-width        2))
  :hook   (sh-mode . gker-setup-sh-mode))

;; C/C++/Java Shit
(use-package cc-mode
  :config (defun mine/cc-setup ()
            (setq tab-width     4
                  c-style-alist (cons
                                  '("EXT" (c-basic-offset   . 4)
                                          (tab-width        . 4)
                                          (indent-tabs-mode . t)
                                          (c-offsets-alist  (statement-cont        . (c-lineup-cascaded-calls
                                                                                      c-lineup-assignments))
                                                            (arglist-cont-nonempty . (c-lineup-gcc-asm-reg
                                                                                      c-lineup-cascaded-calls
                                                                                      c-lineup-argcont
                                                                                      c-lineup-arglist))))
                                  c-style-alist)))
  :hook   (c-mode-common . mine/cc-setup)
  :bind   (:map c-mode-base-map
           ("C-c SPC" . align))
  :custom ((c-basic-offset  4)
           (c-default-style "EXT")))

;; JVM Shit
  ;; Java Shit
(use-package lsp-java
  :mark-selected t)
(use-package javadoc-lookup
  :mark-selected t
  :config        (javadoc-add-roots "/usr/lib/jvm/java-8-openjdk-amd64/docs/api/")
                 ;; (javadoc-add-roots "/usr/local/java/jdk1.8.0_45/api")
                 ;; (javadoc-add-roots "/usr/local/java/jdk1.8.0_45/api-junit")
                 ;; (javadoc-add-roots "/usr/local/java/jdk1.8.0_45/api-javaslang")
                 ;; (javadoc-add-roots "/usr/local/java/jdk1.8.0_45/api-commonsCodec"))
  )
(use-package java
  :bind (:map java-mode-map
         ("C-h j"   . javadoc-lookup)
         ("C-x j i" . javadoc-add-import)
         ("C-x j s" . javadoc-sort-imports)
         ("("       . self-insert-command)
         (")"       . self-insert-command)
         (";"       . self-insert-command)
         (","       . self-insert-command)))
(use-package java-repl
  :bind ("C-x j r" . run-java))

  ;; Android Shit
;; (setq android-mode-builder (quote gradle)
;;       android-mode-sdk-dir "~/.android-sdk")
;; (defun android-init ()
;;   (interactive)

;;   (call-interactively 'meghanada-mode)
;;   (call-interactively 'android-mode))
;; (defun android-global-create-project ()
;;   (interactive)

;;   (android-mode t)
;;   (call-interactively 'android-create-project))
;; (global-set-key (kbd "C-x a C-f") 'android-global-create-project)
;; (global-set-key (kbd "C-x a o s") 'android-logcat)

;; (eval-after-load 'android-mode
;;   (lambda ()
;;     (add-hook 'android-mode-hook (lambda ()
;;                                    (local-set-key (kbd "C-c a b") #'(lambda ()
;;                                                                       (interactive)

;;                                                                       (android-gradle "build")))
;;                                    (local-set-key (kbd "C-c l s") 'android-logcat-set-filter)
;;                                    (local-set-key (kbd "C-c l c") 'android-logcat-clear-filter)))))

;; ;; (eval-after-load 'android-mode
;; ;;   (lambda ()
;; ;;     (add-hook 'android-mode-hook (lambda ()
;; ;;                                    (setq compile-command "gradle compileDebugAidl")

;; ;;                                    (defun android-compile-project ()
;; ;;                                      (interactive)

;; ;;                                      (save-some-buffers)
;; ;;                                      (shell-command (concat
;; ;;                                                       compile-command
;; ;;                                                       " "
;; ;;                                                       (if (= (string-match "gradle_*" compile-command) 0)
;; ;;                                                           "-p "
;; ;;                                                         "")
;; ;;                                                       (android-root)
;; ;;                                                       " &") "*compilation*"))
;; ;;                                    (defun android-gradle-build-apk ()
;; ;;                                      (interactive)

;; ;;                                      (save-some-buffers)
;; ;;                                      (shell-command (concat
;; ;;                                                       "gradle compileDebugAidl assembleRelease -p "
;; ;;                                                       (android-root)
;; ;;                                                       " &") "*compilation*"))
;; ;;                                    (defun android-start-genymotion-emulator (device_name)
;; ;;                                      (interactive "sAndroid Virtual Device Name: ")

;; ;;                                      (if (executable-find "player")
;; ;;                                          (call-process-shell-command
;; ;;                                            (concat (executable-find "player") " --vm-name " device_name " &")
;; ;;                                            nil
;; ;;                                            0)
;; ;;                                        (message
;; ;;                                          "Genymotion or the corresponding command \"player\" is either not "
;; ;;                                          "installed or not within your PATH.")))

;; ;;                                    (local-set-key (kbd "C-x a c")     'android-compile-project)
;; ;;                                    (local-set-key (kbd "C-x a b")     'android-gradle-build-apk)
;; ;;                                                      ;; C-c a g
;; ;;                                    (local-set-key (kbd "C-x a m")     'android-start-genymotion-emulator)
;; ;;                                    (local-set-key (kbd "C-x a o f s") 'android-logcat-set-filter)
;; ;;                                    (local-set-key (kbd "C-x a o f c") 'android-logcat-clear-filter)))))

;; (add-hook 'gud-mode-hook (lambda ()
;;                            (add-to-list
;;                              'gud-jdb-classpath
;;                              "~/.android-sdk/platforms/android-30/android.jar")))

;; Elixir Shit
(use-package flymake-elixir
  :mark-selected t
  :hook          (elixir-mode . flymake-elixir-load))
(use-package elixir-mode
  :mark-selected t)

  ;; ElixirLS Shit
(use-package lsp-elixir
  :config (defvar lsp-elixir--config-options (make-hash-table))
          ;; (puthash "dialyzerEnabled" :json-false lsp-elixir--config-options)

          (defun mine/lsp-elixir-setup ()
            (lsp--set-configuration `(:elixirLS ,lsp-elixir--config-options)))
  :hook   (lsp-after-initialize . mine/lsp-elixir-setup)
  :custom (lsp-elixir-server-command '("~/elixir-ls/release/erl25/language_server.sh"))
          (lsp-elixir-suggest-specs  nil))

;; SQL Shit
(use-package clomacs
  :custom (clomacs-httpd-default-port 8090 "Use a port other than 8080"))
(use-package ejc-sql
  :mark-selected t
  :config        (ejc-create-connection
                   "Swanye Local"
                   :dependencies   [[org.mariadb.jdbc/mariadb-java-client "3.4.0"]]
                   :classname      "org.mariadb.jdbc.Driver"
                   :connection-uri "jdbc:mariadb://localhost:3306/hubadub_elixir"
                   :user           "jude"
                   :password       "judehub"
                   ;; :connection-uri (concat
                   ;;                   "jdbc:mariadb://192.168.1.5:3306;"
                   ;;                   "databaseName=hubadub_elixir;"
                   ;;                   "user=judehub;"
                   ;;                   "password=jude;")
                   ;; :subprotocol "mariadb"
                   ;; :subname     "//192.168.1.5:3306/hubadub_elixir"
                   ;; :user        "judehub"
                   ;; :password    "jude"
                   )

                 ;; (ejc-create-connection
                 ;;   "RADEL"
                 ;;   :dependencies   [[org.mariadb.jdbc/mariadb-java-client "2.6.0"]]
                 ;;   :classname      "org.mariadb.jdbc.Driver"
                 ;;   :connection-uri "jdbc:mariadb://localhost:3306/radel"
                 ;;   :user           "dict"
                 ;;   :password       "dictionary")

                 (defun mine/ejc-sql-setup ()
                   (ejc-set-fetch-size         1000)
                   (ejc-set-max-rows           1000)
                   (ejc-set-column-width-limit nil)
                   (ejc-set-use-unicode        t))
  :hook          ((ejc-sql-minor-mode . ejc-ac-setup)
                  (ejc-sql-minor-mode . ejc-eldoc-setup)
                  (ejc-sql-minor-mode . mine/ejc-sql-setup)))

;; Version Control
;; (setq magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1)
(use-package magit
  :mark-selected t
  :config        (defun mine/magit-status-choose (git-directory)
                   (interactive (list (counsel-read-directory-name "Git repository: ")))

                   (magit git-directory))
  :bind          (("C-x m"   . magit-status)
                  ("C-x M"   . mine/magit-status-choose)
                  ("C-c m l" . magit-log-buffer-file)
                  ("C-c m d" . magit-diff-buffer-file))
  :custom        (magit-display-buffer-function          #'magit-display-buffer-same-window-except-diff-v1)
                 (magit-section-initial-visibility-alist '((unstaged . show)
                                                           (unpushed . show)
                                                           (unpulled . show)
                                                           (modules  . show)
                                                           (stashes  . show)))
                 (git-commit-major-mode                  #'org-mode)
                 (magit-status-sections-hook             '(magit-insert-status-headers                 magit-insert-merge-log
                                                           magit-insert-rebase-sequence                magit-insert-am-sequence
                                                           magit-insert-sequencer-sequence             magit-insert-bisect-output
                                                           magit-insert-bisect-rest                    magit-insert-bisect-log
                                                           magit-insert-untracked-files                magit-insert-unstaged-changes
                                                           magit-insert-staged-changes                 magit-insert-modules
                                                           magit-insert-stashes                        magit-insert-unpushed-to-pushremote
                                                           magit-insert-unpushed-to-upstream-or-recent magit-insert-unpulled-from-pushremote
                                                           magit-insert-unpulled-from-upstream))
                 (magit-module-sections-nested           nil))



;; Miscellaneous Shit
;; Audio Shit
  ;; Music Shit
(use-package ivy-youtube
  :mark-selected t
  :bind          ("C-c b y" . mine/unnecessary-yt)
  :config        (defun mine/unnecessary-yt ()
                   (interactive)

                   (if (boundp 'ivy-youtube-key)
                       (ivy-youtube)
                     (progn
                       (run-at-time
                         "1 sec"
                         nil
                         (lambda ()
                           (bongo-start)))
                       (run-at-time
                         "1 sec"
                         nil
                         (lambda ()
                           (setq ivy-youtube-key (with-temp-buffer
                                                   (insert-file-contents "~/.yt-helm")
                                                   (buffer-string)))
                           (defun ivy-youtube-playvideo (video-url)
                             "Format the youtube URL via VIDEO-ID."
                             (setq uri (string-trim
                                         (shell-command-to-string
                                           (concat "yt-dlp -f140 -g " video-url))))
                             (bongo-insert-uri
                               uri
                               (read-string (concat "Title (default `" uri "'): ") nil nil uri)))
                           (ivy-youtube)))
                       (ivy-youtube)))))
(use-package bongo
  :mark-selected t
  :config        (defun mine/bongo-play-pause ()
                   (interactive)

                   (condition-case err
                       (bongo-set-backend-for-track 'vlc)
                     (lambda () 1))

                   (bongo-dwim))
  :bind          (("C-c b b"   . bongo)
                  ("C-c b SPC" . bongo-pause/resume)
                  ("C-c b s"   . bongo-seek)
                  ("C-c b f"   . bongo-seek-forward)
                  :map bongo-mode-map
                  ("RET" . mine/bongo-play-pause)))

  ;; Podcast Shit
(use-package elfeed-search
  :config (defun mine/elfeed-podcast-insert ()
            (interactive)

            (if-let* ((entry            (elfeed-search-selected :single))
                      (tags  (member 'podcast (elfeed-entry-tags entry))))
                (let ((feed-name (elfeed-feed-title (elfeed-entry-feed       entry)))
                      (title                        (elfeed-entry-title      entry))
                      (urls                         (elfeed-entry-enclosures entry))
                      (bongo                        (bongo-playlist)))
                  (if-let ((video (delq nil (mapcar (lambda (data)
                                                      (and (string-prefix-p "video/"
                                                                            (cadr data))
                                                           data)) urls))))
                      (progn
                        (goto-char (point-max))
                        (bongo-insert-uri (caar video) (concat feed-name " ___ " title))

                        (unless (bongo-playing-p)
                          (forward-line -1)
                          (bongo-dwim))

                        (bongo-recenter)
                        (previous-buffer)
                        (bury-buffer bongo)

                        (elfeed-search-untag-all-unread))
                    (if-let ((audio (delq nil (mapcar (lambda (data)
                                                        (and (string-prefix-p "audio/"
                                                                              (cadr data))
                                                             data)) urls))))
                        (progn
                          (goto-char (point-max))
                          (bongo-insert-uri (caar audio) (concat feed-name " ___ " title))

                          (unless (bongo-playing-p)
                            (forward-line -1)
                            (bongo-dwim))

                          (bongo-recenter)
                          (previous-buffer)
                          (bury-buffer bongo)

                          (elfeed-search-untag-all-unread))
                      (message "No audio or video to add to Bongo!"))))
              (message "Not a podcast!")))

          (defun mine/elfeed-search-entry-eww ()
            (interactive)

            (let ((entry (elfeed-search-selected :single)))
              (elfeed-search-untag-all-unread)
              (eww (elfeed-entry-link entry) t)
              (reading-mode)))
  :bind   (:map elfeed-search-mode-map
           ("g" . elfeed-update)
           ("l" . recenter-top-bottom)
           ("e" . mine/elfeed-search-entry-eww)
           ("i" . mine/elfeed-podcast-insert)
           ("m" . bongo-playlist)
           ("v" . scroll-up-command)))

(use-package elfeed-show
  :config (defun mine/elfeed-show-entry-eww ()
            (interactive)

            (eww (elfeed-entry-link elfeed-show-entry) t)
            (reading-mode))
  :bind   (:map elfeed-show-mode-map
           ("N" . elfeed-show-next)
           ("P" . elfeed-show-prev)
           ("B" . elfeed-show-visit)
           ("E" . mine/elfeed-show-entry-eww)

           ("n" . next-line)
           ("f" . forward-char)
           ("b" . backward-char)
           ("p" . previous-line)

           ("a" . move-beginning-of-line)
           :map shr-map
           ("a" . move-beginning-of-line)
           ("A" . shr-show-alt-text)
           :map elfeed-show-mode-map
           ("e" . move-end-of-line)

           ("v" . scroll-up-command)

           ("l" . recenter-top-bottom)

           ("SPC" . set-mark-command)))

(use-package elfeed
  :mark-selected t
  :hook          (elfeed-new-entry . mine/elfeed-podcast-tagger)
  :config        (defun mine/elfeed-podcast-tagger (entry)
                   (when (seq-find (lambda (mime) (string-match-p (regexp-quote "audio/") mime))
                                   (mapcar #'cadr (elfeed-entry-enclosures entry)))
                     (elfeed-tag entry 'podcast)))
  :custom        (elfeed-feeds '("https://archive81.libsyn.com/rss"
                                 "http://feeds.nightvalepresents.com/aliceisntdeadpodcast"
                                 "https://dugganhill.com/the-radio-play?format=rss"
                                 "https://voicefromdarkness.libsyn.com/rss"
                                 "https://primordialdeep.libsyn.com/rss"
                                 "https://mirrorspodcast.libsyn.com/rss"
                                 "https://omnycontent.com/d/playlist/e73c998e-6e60-432f-8610-ae210140c5b1/E5F91208-CC7E-4726-A312-AE280140AD11/D64F756D-6D5E-4FAE-B24F-AE280140AD36/podcast.rss"
                                 "https://www.youtube.com/feeds/videos.xml?channel_id=UCOAsPwoHjxt3vzdqo2OPqIw"
                                 "https://rigsbywi.com/feed/"
                                 "https://newrepublic.com/rss.xml"
                                 "https://prospect.org/api/rss/all.rss"
                                 "https://haitiantimes.com/feed/"
                                 "https://haitiliberte.com/feed/"
                                 "https://mayfirst.coop/en/audio/index.xml")))

;; Chat Shit
  ;; IRC
(use-package lui
  :config (defun mine/circe-setup ()
            (setq word-wrap   t
                  wrap-prefix (make-string 22 ? )))
  :hook   (lui-mode . mine/circe-setup)
  :custom (lui-fill-type           nil)
          (lui-time-stamp-position 'left)
          (lui-time-stamp-format   "%b %d ∵ %I:%M %p "))

(use-package circe-notifications
  :mark-selected t
  :hook          (circe-server-connected . enable-circe-notifications)
  :custom        (circe-notifications-wait-for    0)
                 (circe-notifications-alert-style 'dunst))

(use-package circe-color-nicks
  :custom (circe-color-nicks-everywhere t))

(use-package circe
  :mark-selected t
  :config        (enable-circe-color-nicks)
  :custom        (circe-format-server-topic    "*** Topic change by {userhost}: {topic-diff}")
                 (circe-format-say             "··· {nick}
{body}
")
                 (circe-format-self-say        "> {body}\n")
                 (circe-prompt-string          (concat "                  "
                                                       (propertize ">" 'face 'circe-prompt-face)
                                                       " "))
                 (circe-network-options        (let ((nick "tomenzgg"))
                                                 `(("Libera Chat" :nick               ,nick
                                                                  :channels           ("#guix" "#nonguix" "#guile" "#swanye")
                                                                  :nickserv-password  ,(if-let ((match (car (auth-source-search
                                                                                                              :user nick
                                                                                                              :host "irc.libera.chat"))))
                                                                                           (let ((secret (plist-get match :secret)))
                                                                                             (if (functionp secret) (funcall secret) secret))
                                                                                         (error "Password not found for %S" nick))
                                                                  :server-buffer-name "⇄ Libera Chat")
                                                   ("bitlbee" :host                           "localhost"
                                                              :service                        "6667"
                                                              :nick                           "jean"
                                                              :nickserv-password              "jean"
                                                              ;; below stuff enables Circe to actually authenticate with Bitlbee
                                                              :nickserv-mask                  "\\(bitlbee\\|root\\)!\\(bitlbee\\|root\\)@"
                                                              :nickserv-identify-challenge    "use the \x02identify\x02 command to identify yourself"
                                                              :nickserv-identify-command      "PRIVMSG &bitlbee :identify {password}"
                                                              :nickserv-identify-confirmation "Password accepted, settings and accounts loaded"
                                                              :channels                       ("&bitlbee")
                                                              ;; necessary stuff for authentication ends
                                                              :server-buffer-name             "⇄ BitlBee")))))

;; (load "pinentry")
;; (pinentry-start)
;; (setq epg-gpg-program "gpg")

;; Browser Shit
(use-package browse-url
  :init   (defun browse-url-qutebrowser (url &optional new-window)
            "Open URL in Qutebrowser"
            (interactive (browse-url-interactive-arg "URL: "))

            (start-process (concat "qutebrowser " url) nil "qutebrowser"
                                                           (concat ":open -w " url)))
       ;; (browse-url-browser-function 'browse-url-firefox)
  :custom (browse-url-browser-function 'browse-url-qutebrowser))

(let ((browse browse-url-browser-function))
  (use-package eww
    :bind   (:map eww-mode-map
             ("M-d" . eww)
             ("M-D" . (lambda () (interactive) (eww-current-url)))
             ("C-n" . (lambda () (interactive) (let ((current-prefix-arg 1))
                                                 (call-interactively #'eww))))

             ("C-<return>" . (lambda ()
                               (interactive)

                               (eww-follow-link t)

                               (previous-buffer)))

             ("M-<up>"    . (lambda ()
                              (interactive)

                              (funcall browse (eww-current-url))))
             ("M-<left>"  . eww-back-url)
             ("M-<right>" . eww-forward-url)

             ("[[" . eww-previous-url)
             ("]]" . eww-next-url)

             ("y u" . eww-copy-page-url)

             ("C-d" . eww-add-bookmark)

             ("M-c" . (lambda ()
                        (interactive)

                        (let ((new (clone-buffer (concat (buffer-name) " [clone]"))))
                          (switch-to-buffer new))))

             ("u" . eww-view-source)

             ("n" . next-line)
             ("f" . forward-char)
             ("b" . backward-char)
             ("p" . previous-line)

             ("a" . move-beginning-of-line)
             :map eww-link-keymap
             ("a" . move-beginning-of-line)
             :map shr-image-map
             ("a" . move-beginning-of-line)
             :map eww-link-keymap
             ("A" . shr-show-alt-text)
             :map shr-image-map
             ("A" . shr-show-alt-text)
             :map eww-mode-map
             ("e" . move-end-of-line)

             ("v" . scroll-up-command)
             :map eww-link-keymap
             ("v" . scroll-up-command)
             :map eww-image-link-keymap
             ("v" . scroll-up-command)

             :map eww-mode-map
             ("l" . recenter-top-bottom)
             ("r" . move-to-window-line-top-bottom)

             ("SPC" . set-mark-command))
    :custom (eww-auto-rename-buffer 'title)))

(use-package w3m
  :mark-selected t
  :config        (defun mine/w3m-lnum-follow (arg)
                   (interactive "p")

                   (if (eq major-mode 'mime-view-mode)
                       (cl-letf* (((symbol-function 'w3m-goto-url)               browse-url-browser-function)
                                  ((symbol-function 'w3m-goto-url-new-session)   browse-url-browser-function)
                                  ((symbol-function 'w3m-history-store-position) (lambda ())))
                         (call-interactively #'w3m-lnum-follow))
                     (call-interactively #'w3m-lnum-follow)))

                 (defun mine/w3m-safe-view-this-url (&optional force)
                   (interactive "P")

                   (if (eq major-mode 'mime-view-mode)
                       (cl-letf* (((symbol-function 'w3m-goto-url)             browse-url-browser-function)
                                  ((symbol-function 'w3m-goto-url-new-session) browse-url-browser-function))
                         (call-interactively #'w3m-safe-view-this-url))
                     (call-interactively #'w3m-safe-view-this-url)))
  :bind          (:map w3m-minor-mode-map
                  ("RET" . mine/w3m-safe-view-this-url)
                  ("FL"  . mine/w3m-lnum-follow)))

;; E-mail Shit
(use-package wl-util
  :config (defvar mine/wl-biff-debug t
            "Set to t to enable debug messages from notifier code.")

          (defun mine/wl-diff-notify-summaries (summaries unseen)
            ;;; debug
            (when mine/wl-biff-debug
              (message "notifying %d unseen email" unseen))

            (let ((alert-fade-time 20))
              (unless (featurep 'alert)
                (require 'alert))

              (alert-dunstify-notify
                `(:title   ,(format "You have %d unseen E-mail %s"
                                    unseen
                                    (concat "message" (if (= unseen 1) "" "s")))
                  :message ,summaries))))

          (defun mine/wl-biff-collect-summaries-for-folder (name)
            ;;; debug
            (when mine/wl-biff-debug
              (message "collecting summaries for %s" name))

            (let* ((summaries                              "")
                   (folder    (elmo-get-folder          name))
                   (session   (elmo-imap4-get-session folder)))
              (cl-multiple-value-bind (new unseen total)
                  (elmo-imap4-folder-diff-plugged folder)
                ;; Collect unseen message summaries from each folder into SUMMARIES variable.
                (dotimes (i (min unseen 2))
                  (elmo-imap4-send-command-wait session "SELECT \"INBOX\"")

                  (let* ((number  (- total i))
                         (summary (elmo-imap4-response-bodydetail-text
                                    (elmo-imap4-response-value
                                      (elmo-imap4-send-command-wait
                                        session
                                        (format (concat "FETCH %d (FLAGS "
                                                                  "BODY.PEEK[HEADER.FIELDS "
                                                                            "(DATE FROM SUBJECT)])")
                                                number))
                                      'fetch))))
                    (setf summaries
                          (concat summaries
                                  (replace-regexp-in-string
                                    "^Subject:"
                                    "<u><i>Subject:</i></u>"
                                    (replace-regexp-in-string
                                      "^Date:"
                                      "<u><i>Date:</i></u>"
                                      (replace-regexp-in-string
                                        "^From:"
                                        "<u><i>From:</i></u>"
                                        (string-replace ">"
                                                        "&gt;"
                                                        (string-replace "<"
                                                                        "&lt;"
                                                                        summary))
                                        t)
                                      t)
                                    t)))))
                ;;; debug
                (when mine/wl-biff-debug
                  (message "%d unseen messages for folder %s" unseen name))
                (list summaries unseen))))

          (defun mine/wl-biff-notify ()
            "Hook to add to the wl-biff-notify-hook."

            ;;; debug
            (when mine/wl-biff-debug
              (message "Running mine/wl-biff-notify"))

            ;; If we get called, something must have happened.
            (let ((summaries    "")
                  (total-new     0)
                  (total-unseen  0))
              (dolist (f wl-biff-check-folder-list)
                ;;; debug
                (when mine/wl-biff-debug
                  (message "Collecting summaries for %s." f))

                (cl-multiple-value-bind (new-summaries count)
                    (mine/wl-biff-collect-summaries-for-folder f)
                  (when (> count 0)
                    (setf total-unseen (+ total-unseen count)
                          summaries    (concat summaries new-summaries)))))

              (if (> total-unseen 0)
                  ;; Display message summaries to the user.
                  (mine/wl-diff-notify-summaries summaries total-unseen)
                ;;; debug
                (when fmg-biff-debug
                  (message "No messages to show")))))

          ;; https://gist.github.com/fgilham/e21cbcf27fd70618ad880886981de58a
          ;; https://gist.github.com/QiangF/a66fcf035d11b3d6544fa2f446cc0600
          (defun mine/wl-biff-alert () ;; (alert "New E-mail!")
            (make-thread #'mine/wl-biff-notify))
  :hook   (wl-biff-notify . mine/wl-biff-notify)
  :init   (defun mine/wl-get-inboxes ()
            (mapcar (lambda (title)
                      (let ((attrs (secrets-get-attributes "Login" title)))
                        (concat "%INBOX"
                                (mine/wl-imap-folder (alist-get :user attrs)
                                                     (alist-get :host attrs)
                                                     (alist-get :port attrs)))))
                    (secrets-search-items "Login" :service "imap")))
  :custom (wl-biff-check-interval    90 "Check every minute and a half")
          (wl-biff-use-idle-timer    t  "Check only when idle")
          (wl-biff-check-folder-list (mine/wl-get-inboxes))
          (wl-strict-diff-folders    (mapcar #'regexp-quote (mine/wl-get-inboxes))
            "Use strict diff so wl-biff works with Gmail and others"))

(use-package org-mime
  :mark-selected t
  :custom        ;; for gnus – this is set by default
                 ;; (org-mime-library 'mml)
                 ;; OR for Wanderlust (WL)
                 (org-mime-library 'semi))
(use-package mime-view
  :custom ;; (mime-pgp-decrypt-when-preview      t)
          (mime-view-type-subtype-score-alist
            '(((text . enriched) . 3)
              ((text . richtext) . 2)
              ((text . plain)    . 1)
              ((text . html)     . mime-view-text/html-entity-score)
              (multipart         . mime-view-multipart-entity-score))
            "https://superuser.com/questions/435668/how-to-view-alternate-mime-content-in-wanderlust"))

(use-package elmo-spam
  :custom ;; (elmo-spam-scheme 'sa
          ;;   "sa for spamassassin, see the elmo-spam-scheme docs for alternatives")
          (elmo-spam-scheme 'header))
(use-package wl-spam
  :hook ((wl-folder-init . wl-spam-setup)))

(use-package wl-address
  :custom (wl-user-mail-address-list
            (mapcar (lambda (title)
                      (alist-get :user (secrets-get-attributes "Login" title)))
                    (secrets-search-items "Login" :service "smtp"))
            "You should set this variable if you use multiple e-mail addresses"))

(use-package wl-draft
  :hook   ((wl-draft-reply   . wl-draft-config-exec)
           (wl-draft-forward . wl-draft-config-exec))
  :bind   (:map wl-draft-mode-map
           ("C-l" . recenter-top-bottom))
  :custom (wl-forward-subject-prefix                  "Fwd: ")
          (wl-default-draft-cite-date-format-string   "On %A (%Y, %B %d) at %r %Z")
          (wl-default-draft-cite-header-format-string "%s, %s wrote:\n")
          (wl-fcc-force-as-read                       t
            "Mark sent mail (in the wl-fcc folder) as read")
          (wl-template-alist
            (mapcar (lambda (title)
                      (let* ((attrs   (secrets-get-attributes "Login" title))
                             (address (alist-get :user attrs))
                             (domain  (cadr (string-split address "@")))
                             (name    (alist-get :name attrs))
                             (server  (alist-get :host attrs))
                             (imap    (secrets-get-attributes
                                        "Login"
                                        (car (secrets-search-items "Login"
                                                                   :service "imap"
                                                                   :host    (alist-get :imap
                                                                                       attrs)))))
                             (port    (alist-get :port attrs))
                             (sent    (alist-get :sent attrs))
                             (foldend (concat "%%%s"
                                              (mine/wl-imap-folder (alist-get :user imap)
                                                                   (alist-get :host imap)
                                                                   (alist-get :port imap)))))
                        `(,address
                          (wl-from                   . ,(concat name " <" address ">"))
                          ("From"                    . wl-from)
                          (wl-smtp-posting-user      . ,(alist-get :user imap))   ; User's address
                          (wl-smtp-posting-server    . ,server)                   ; SMTP server
                          (wl-message-id-domain      . ,server)                   ; And... Again?
                          (wl-smtp-authenticate-type . "login")                   ; Auth. type
                          (wl-smtp-connection-type   . 'starttls)                 ; Use TLS
                          (wl-smtp-posting-port      . ,(string-to-number port))  ; The SMTP port
                          (wl-local-domain           . ,domain)
                          (wl-draft-folder           . "+draft")
                          (wl-fcc                    . ,(format foldend sent))
                          ("Fcc"                     . wl-fcc)
                          (mime-edit-pgp-processing  . ,(if (string= server "mail.mayfirst.org")
                                                            (quote '(sign))
                                                          nil))
                          (body                      . "\n\n"))))
                    (secrets-search-items "Login" :service "smtp"))
            "choose template with C-c C-j")
          (wl-draft-config-alist
            (apply #'append
                   (mapcar (lambda (title)
                             (let* ((attrs   (secrets-get-attributes "Login" title))
                                    (address (alist-get :user attrs))
                                    (imap    (secrets-get-attributes
                                               "Login"
                                               (car (secrets-search-items "Login"
                                                                          :service "imap"
                                                                          :host    (alist-get :imap
                                                                                              attrs)))))
                                    (foldend (concat (regexp-quote
                                                       (mine/wl-imap-folder address
                                                                            (alist-get :host imap)
                                                                            (alist-get :port imap)))
                                                     "$"))
                                    (to2from (concat "^\\(To\\|Cc\\|Delivered-To\\): .*"
                                                     (apply #'concat
                                                            (mapcar (lambda (char)
                                                                      (concat "["
                                                                              `(,(  upcase char)
                                                                                ,(downcase char))
                                                                              "]"))
                                                                    address)))))
                               `((reply   ,to2from                               (template . ,address))
                                 (forward ,to2from                               (template . ,address))
                                 ((string-match ,foldend wl-draft-parent-folder) (template . ,address)))))
                           (secrets-search-items "Login" :service "smtp")))))

(use-package wl-message
  :custom (wl-message-sort-field-list           '("^From"
                                                  "^Organization:"
                                                  "^X-Attribution:"
                                                  "^Subject"
                                                  "^Date"
                                                  "^To"
                                                  "^Cc"
                                                  "^Bcc"))
          (wl-message-visible-field-list        '("^\\(To\\|Cc\\|Bcc\\):"
                                                  "^Subject:"
                                                  "^\\(From\\|Reply-To\\):"
                                                  "^Organization:"
                                                  "^Message-I[dD]:"
                                                  "^\\(Posted\\|Date\\):"
                                                  "^User-Agent:"
                                                  ;; "^MIME-Version:"
                                                  ))
          (wl-message-ignored-field-list        '("^.*:")
            "Hide many fields from message buffers")
          (wl-message-buffer-prefetch-threshold nil
            "Always download emails without confirmation"))

(use-package wl-summary
  :init   (defun mine/wl-imap-folder (address server port)
            (concat ":\""       address
                    "\"/clear@" server
                    ":"         port    "!"))
  :hook   ((wl-summary-prepared-pre . mine/wl-summary-folders-setup)
           (wl-summary-prepared-pre . wl-summary-define-mark-action))
  :config (defun mine/wl-summary-folders-setup ()
            (setq wl-quicksearch-folder wl-summary-buffer-folder-name
                  wl-spam-folder        (seq-reduce (lambda (result title)
                                                      (let* ((attrs   (secrets-get-attributes "Login" title))
                                                             (address                (alist-get :user attrs)))
                                                        (if (string-match (regexp-quote address)
                                                                          wl-summary-buffer-folder-name)
                                                            (concat "%"
                                                                    (alist-get :spam attrs)
                                                                    (mine/wl-imap-folder address
                                                                                         (alist-get :host
                                                                                                    attrs)
                                                                                         (alist-get :port
                                                                                                    attrs)))
                                                          result)))
                                                    (secrets-search-items "Login" :service "imap")
                                                    nil)))

          (defun mine/wl-summary-mark-as-read ()
            (interactive)

            (wl-summary-mark-as-read)
            (forward-line))
  :bind   (:map wl-summary-mode-map
           ("n"       . next-line)
           ("N"       . wl-summary-cursor-down)
           ("M-n"     . wl-summary-next)
           ("p"       . previous-line)
           ("P"       . wl-summary-cursor-up)
           ("M-p"     . wl-summary-prev)
           ;; previously 'wl-summary-goto-last-displayed-msg
           ("TAB"     . wl-thread-open-close)
           ("C-<tab>" . wl-thread-open-children)
           ("a"       . wl-summary-reply-with-citation)
           ("A"       . wl-summary-reply)
           ("="       . wl-summary-toggle-disp-msg)
           ("v"       . scroll-up-command)
           ("R"       . mine/wl-summary-mark-as-read)
           ("M-w"     . kill-ring-save)
           ("/"       . wl-quicksearch-goto-search-folder-wrapper))
  :custom (wl-summary-line-format           "%n┃%T%P│%Y-%M-%D ⟨%h:%m⟩║ %t%[%17(%c %f%) %] %s")
          (wl-summary-width                 nil)
          (wl-summary-fix-timezone          (cadr (current-time-zone)))
          (wl-summary-default-number-column 6)
          (wl-summary-order                 'descending)
          (wl-summary-sort-specs            '(date number subject from list-info size))
          (wl-summary-auto-sync-marks       nil
            "make things faster? https://www.emacswiki.org/emacs/WlFaq#h5o-10")
          (wl-dispose-folder-alist
            (append (mapcar (lambda (title)
                              (let* ((attrs   (secrets-get-attributes "Login" title))
                                     (address (alist-get :user attrs)))
                                `(,(concat "^%.*" (regexp-quote address)) .
                                  ,(concat "%"
                                           (alist-get :trash attrs)
                                           (mine/wl-imap-folder address
                                                                (alist-get :host attrs)
                                                                (alist-get :port attrs))))))
                            (secrets-search-items "Login" :service "imap"))
                    '(("^-" . remove)
                      ("^@" . remove)))))

(use-package wl
  :hook   ((mime-view-mode  . mine/wl-message-setup)
           (wl-summary-mode . mine/wl-summary-setup))
  :config (defun mine/wl-summary-setup ()
            (face-remap-add-relative 'hl-line '(:background "#333333"))
            (hl-line-mode t))
          (defun mine/wl-message-setup ()
            (local-set-key (kbd "TAB")       #'w3m-next-anchor)
            (local-set-key (kbd "<backtab>") #'w3m-previous-anchor)
            (local-set-key (kbd "=")         #'mine/wl-message-summary-toggle-disp-msg)
            (local-set-key (kbd "V")         #'mime-preview-launch-current-entity)

            (local-set-key (kbd "n") #'next-line)
            (local-set-key (kbd "N") #'mime-preview-move-to-next)
            (local-set-key (kbd "f") #'forward-char)
            (local-set-key (kbd "b") #'backward-char)
            (local-set-key (kbd "p") #'previous-line)
            (local-set-key (kbd "P") #'mime-preview-move-to-previous)

            (local-set-key (kbd "a") #'move-beginning-of-line)
            (local-set-key (kbd "A") #'mime-preview-follow-current-entity)
            (local-set-key (kbd "e") #'move-end-of-line)
            (local-set-key (kbd "E") #'mime-preview-extract-current-entity)

            (local-set-key (kbd "v") #'scroll-up-command)

            (local-set-key (kbd "l") #'recenter-top-bottom)
            (local-set-key (kbd "L") #'wl-message-toggle-disp-summary)
            (local-set-key (kbd "r") #'move-to-window-line-top-bottom)

            (local-set-key (kbd "SPC") #'set-mark-command)

            (local-set-key (kbd "<") #'beginning-of-buffer)
            (local-set-key (kbd ">")       #'end-of-buffer))
          (defun mine/wl-message-summary-toggle-disp-msg ()
            (interactive)

            (when-let ((wind (get-buffer-window "Summary")))
              (select-window wind)

              (wl-summary-toggle-disp-msg)))
          (defun mime-preview-launch-current-entity (&optional _ignore-examples)
            "Launch current entity by the appropriate program (maybe).
It decodes current entity to a temporary file and then loads said file via
'exo-open'."
            (interactive "P")

            (let ((entity (get-text-property (point) 'mime-view-entity)))
              (when-let ((entity)
                         (filename (concat "/tmp/"
                                           (file-name-nondirectory
                                             (or (mime-entity-safe-filename entity)
                                                 (format "%s"
                                                         (mime-entity-media-type entity)))))))
                (mime-write-entity-content entity filename)

                (call-process-shell-command (concat "exo-open "
                                                    (shell-quote-argument filename))
                                            nil
                                            0))))

          ;; For non ascii-characters in folder-names
          (setq elmo-imap4-use-modified-utf7 t)

          (defun mine/wl-switch ()
            (interactive)

            (if-let ((summary (get-buffer "Summary")))
                (switch-to-buffer summary)
              (if-let ((folder (get-buffer "Folder")))
                  (switch-to-buffer folder)
                (wl))))
  :bind   (("C-c E" . mine/wl-switch))
  :custom (elmo-passwd-storage-type                   'auth-source
            "Use auth-source; in our case, Gnome keyring")
          (elmo-network-session-idle-timeout          110
            "make things faster? https://www.emacswiki.org/emacs/WlFaq#h5o-10")
          (wl-default-spec                            "%"
            "For auto-completion")
          (wl-prefetch-threshold                      nil
            "Always download emails without confirmation")
          (elmo-message-fetch-threshold               nil
            "Always download emails without confirmation")
          (wl-from
            (alist-get :user
                       (secrets-get-attributes
                         "Login"
                         (car (secrets-search-items "Login"
                                                    :service "imap"))))))

(use-package wanderlust
  :mark-selected t)
